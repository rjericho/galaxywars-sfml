#include "Planet.h"
#include "Battleship.h"
#include "Transportship.h"

CPlanet::CPlanet() 
{
	CObj("Default Base Name", "Default base Description");
	ships = new vector<CSpaceship*>(0);
}

CPlanet::CPlanet(const CSpaceship &value, vector<CSpaceship*>*incoming) : CSpaceship(value)
{
	this->setAllShips(incoming);
}

CPlanet::CPlanet(const CPlanet &value) : CSpaceship(value)
{
	this->setAllShips(value.getAllShips());
}

CPlanet::~CPlanet()
{
	for (vector<CSpaceship*>::iterator itr = ships->begin(); itr != ships->end(); itr++)
		delete *itr;
	delete ships;
}

vector<CSpaceship*>* CPlanet::getAllShips() const
{
	return this->ships;
}

void CPlanet::setAllShips(vector<CSpaceship*>* value)
{
	this->ships = value;
	for each(CSpaceship* current in *ships)
	{
		this->setArmor(this->getArmor() + current->getArmor());
		this->setSpeed(this->getSpeed() + current->getSpeed());
	}
}

void CPlanet::putShip(CSpaceship* value)
{
	this->ships->push_back(value);
	this->setArmor(this->getArmor() + value->getArmor());
	this->setSpeed(this->getSpeed() + value->getSpeed());
	if (this->amount.find(value->getName()) != this->amount.end())
		this->amount[value->getName()]++;
	else
		this->amount.insert(pair<string, int>(value->getName(), 1));
}

string CPlanet::planetShipsInfo()
{
	//string res = CSpaceship::getObjInfo();
	string res = "";
	for each(CSpaceship* current in *ships)
	{
		res += current->getObjInfo() + "\n================================\n";
	}
	return res;
}

void CPlanet::recount()
{
	this->amount.clear();
	for each(CSpaceship* current in *ships)
	{
		if (this->amount.find(current->getName()) != this->amount.end())
			this->amount[current->getName()]++;
		else
			this->amount.insert(pair<string, int>(current->getName(), 1));
	}
}

string CPlanet::getTypes()
{
	string res;
	for each (pair<string, int> current in amount)
	{
		res += "Ship Name: " + current.first + " Amount: " + to_string(current.second) + "\n";
	}
	return res;
}

void CPlanet::deleteShipByName(string value)
{
	for (vector<CSpaceship*> ::iterator itr = ships->begin(); itr != ships->end();)
	{
		if ((*itr)->getName() == value)
		{
			itr = ships->erase(itr);
		}
		else
		{
			itr++;
		}
	}
	this->recount();
}

bool compare(CSpaceship *first, CSpaceship *second)
{
	CBattleship *left = dynamic_cast <CBattleship*> (first);
	CTransportship *right = dynamic_cast <CTransportship*> (second);
	if (left && right)
		return 1;
	else 
		return first->getName() < second->getName();
		
}

void CPlanet::sortAll()
{
	sort(this->ships->begin(), this->ships->end(), &compare);
}
