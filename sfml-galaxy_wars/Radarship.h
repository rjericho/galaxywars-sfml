#pragma once
#include "Spaceship.h"
class CRadarship :
	public CSpaceship
{
private:
	int visionRadius;
	const int type = 2;

public:
	CRadarship();
	CRadarship(const CRadarship &value);
	CRadarship(CSpaceship *value, int new_radius);
	virtual ~CRadarship();

public:
	virtual void setVisionRadius(const int value);

	virtual int getVisionRadius() const;


public: virtual map< string, string > *readOptions();

public:
	virtual string getObjInfo();
	virtual CSpaceship* makeDuplicate();
};

