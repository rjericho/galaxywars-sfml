#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>

#include "Spaceship.h"

#include "IDuplicator.h"
#include "IGetInform.h"

using namespace std;

bool compare(CSpaceship *left, CSpaceship *right);

class CPlanet :
	public CSpaceship,
	public CObj
{
protected:
	vector<CSpaceship*>* ships;
	map <string, int> amount;

public:
	CPlanet();
	CPlanet(const CSpaceship &value, vector<CSpaceship*>*incoming);
	CPlanet(const CPlanet &value);
	virtual ~CPlanet();

public:
	virtual void setAllShips(vector<CSpaceship*>*value);
	vector<CSpaceship*>* getAllShips() const;

	void putShip(CSpaceship* value);
	virtual string planetShipsInfo();

	void recount();
	string getTypes();
	virtual void deleteShipByName(string value);
	void sortAll();
};

