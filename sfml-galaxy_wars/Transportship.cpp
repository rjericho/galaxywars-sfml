#include "Transportship.h"


CTransportship::CTransportship() : CSpaceship()
{
	this->setType(3);
	this->setPassengerAmount(100);
	this->isEmpty = 0;

}

CTransportship::CTransportship(const CTransportship &value) : CSpaceship(value)
{
	this->setType(3);
	this->setPassengerAmount(value.getPassengerAmount());
}

CTransportship::CTransportship(CSpaceship *value, int new_passengersAmount) : CSpaceship(*value)
{
	this->setType(3);
	this->setPassengerAmount(new_passengersAmount);

}

CTransportship::~CTransportship()
{

}

void CTransportship::setPassengerAmount(const int value)
{
	this->passengersAmount = value;
}

int CTransportship::getPassengerAmount() const
{
	return this->passengersAmount;
}

void CTransportship::emptyShip()
{
	this->isEmpty = 1;
}

map <string, string> *CTransportship::readOptions()
{
	map <string, string> * file = ((CSpaceship*)this)->readOptions();
	this->setPassengerAmount(stoi(file->find("#PassengerAmount")->second));
	return file;
}

string CTransportship::getObjInfo()
{
	return CSpaceship::getObjInfo() + "#PassengerAmount: " + to_string(this->getPassengerAmount()) + "\n";
}

CSpaceship* CTransportship::makeDuplicate()
{
	CSpaceship *duplicated = new CTransportship(*this);
	return duplicated;
}

CTransportship operator+(const CTransportship& first, const CTransportship& second)
{
	CTransportship *twice = new CTransportship(first);
	twice->setPassengerAmount(twice->getPassengerAmount() + second.getPassengerAmount());
	twice->setArmor(twice->getArmor() + second.getArmor());
	return *twice;
}