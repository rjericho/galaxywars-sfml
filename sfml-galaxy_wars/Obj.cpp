#include "Obj.h"
using namespace std;

CObj::CObj()
{
	this->setName("Default Initialization");
	this->setDescription("Default Initialization");
}

CObj::CObj(const CObj &in_object)
{
	this->setName(in_object.getName());
	this->setDescription(in_object.getDescription());
}

CObj::CObj(const string in_name, const string in_description)
{
	this->setName(in_name);
	this->setDescription(in_description);
}

CObj::~CObj()
{

}

void CObj::setName(const string &new_name)
{
	name = new_name;
}

void CObj::setDescription(const string &new_description)
{
	description = new_description;
}

string CObj::getName()const
{
	return this->name;
}

string CObj::getDescription()const
{
	return this->description;
}

map< string, string >* CObj::readOptions()
{
	fstream in_stream;
	string par_name, par_value;
	map< string, string > *file = new map<string, string>();

	in_stream.open(this->getName() + ".txt", std::fstream::in);
	while (!in_stream.eof())
	{
		in_stream >> par_name >> par_value;
		file->emplace(pair<string, string>(par_name, par_value));
	}
	in_stream.close();
	this->setName(file->find("#Name")->second);
	this->setDescription(file->find("#Description")->second);

	return file;
}

string CObj::getObjInfo()
{
	return "#ObjectName: " + this->getName() + "\n#ObjectDescription: " + this->getDescription() + "\n";
}