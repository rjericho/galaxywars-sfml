#pragma once

#include "Spaceship.h"

class CBattleship :
	public CSpaceship
{
private:
	int damage;
	int shootSpeed;
	const int type = 1;
public:
	CBattleship();
	CBattleship(const CBattleship &value);
	CBattleship(CSpaceship *value, int new_damage, int new_shootspeed);
	virtual ~CBattleship();

public:
	virtual void setDamage(const int value);
	virtual void setShootSpeed(const int value);

	virtual int getDamage() const;
	virtual int getShootSpeed() const;

public: virtual map< string, string > *readOptions();

public:
	virtual string getObjInfo();
	virtual CSpaceship* makeDuplicate();

public:		friend CBattleship operator+(const CBattleship& first, const CBattleship& second);
};

