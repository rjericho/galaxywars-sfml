#pragma once

#include "Spaceship.h"

class CTransportship :
	public CSpaceship
{
private:
	bool isEmpty;
	int passengersAmount;
public:
	CTransportship();
	CTransportship(const CTransportship &value);
	CTransportship(CSpaceship *value, int new_passengersAmount);
	virtual ~CTransportship();

public:
	virtual void setPassengerAmount(const int value);

	virtual int getPassengerAmount() const;

private:
	void emptyShip();

public: virtual map< string, string > *readOptions();

public:
	virtual string getObjInfo();
	virtual CSpaceship* makeDuplicate();

public:		friend CTransportship operator+(const CTransportship& first, const CTransportship& second);
};

