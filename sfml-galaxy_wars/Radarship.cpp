#include "Radarship.h"


CRadarship::CRadarship() : CSpaceship()
{
	this->setType(2);
	this->setVisionRadius(5);
}

CRadarship::CRadarship(const CRadarship &value) : CSpaceship(value)
{
	this->setType(2);
	this->setVisionRadius(value.getVisionRadius());
}

CRadarship::CRadarship(CSpaceship *value, int new_radius) : CSpaceship(*value)
{
	this->setType(2);
	this->setVisionRadius(new_radius);

}

CRadarship::~CRadarship()
{

}

void CRadarship::setVisionRadius(const int value)
{
	this->visionRadius = value;
}

int CRadarship::getVisionRadius() const
{
	return this->visionRadius;
}

map <string, string> *CRadarship::readOptions()
{
	map <string, string> * file = ((CSpaceship*)this)->readOptions();
	this->setVisionRadius(stoi(file->find("#VisionRadius")->second));
	return file;
}

string CRadarship::getObjInfo()
{
	return CSpaceship::getObjInfo() + "#VisionRadius: " + to_string(this->getVisionRadius()) + "\n";
}

CSpaceship* CRadarship::makeDuplicate()
{
	CSpaceship *duplicated = new CRadarship(*this);
	return duplicated;
}
