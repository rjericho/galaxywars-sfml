#include "Spaceship.h"


CSpaceship::CSpaceship() : CObj()
{
	this->setArmor(1);
	this->setSpeed(1);
}

CSpaceship::CSpaceship(const CSpaceship &value) : CObj(value)
{
	this->setArmor(value.getArmor());
	this->setSpeed(value.getSpeed());

}

CSpaceship::CSpaceship(CObj* value, int armor_value, int speed_value) : CObj(*value)
{
	this->setArmor(armor_value);
	this->setSpeed(speed_value);
}

CSpaceship::~CSpaceship()
{

}

void CSpaceship::setArmor(int value)
{
	this->armor = value;
}

void CSpaceship::setSpeed(int value)
{
	this->speed = value;
}

void CSpaceship::setType(int value)
{
	this->type = value;
}

int CSpaceship::getArmor() const
{
	return armor;
}

int CSpaceship::getSpeed() const
{
	return speed;
}

int CSpaceship::getType() const
{
	return type;
}
map< string, string >* CSpaceship::readOptions()
{
	map< string, string > *file = CObj::readOptions();
	this->setArmor(stoi(file->find("#Armor")->second));
	this->setSpeed(stoi(file->find("#Speed")->second));
	return file;
}

string CSpaceship::getObjInfo()
{
	return CObj::getObjInfo() + "#ObjectArmor: " + to_string(this->getArmor()) + " #ObjectSpeed: " + to_string(this->getSpeed()) + "\n";
}

CObj* CSpaceship::makeDuplicate()
{
	CObj *duplicated = new CSpaceship(*this);
	return duplicated;
}

CSpaceship& CSpaceship::operator--()
{
	this->setArmor(this->getArmor() - 1);
	return *this;
}