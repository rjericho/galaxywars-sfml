#pragma once
#include "Obj.h"

class IDuplicator
{
public:		virtual CObj* makeDuplicate() = 0;
};

