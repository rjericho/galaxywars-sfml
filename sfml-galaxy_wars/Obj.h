#include <iostream>
#include <fstream>
#include <map>
#include <string>


#include "IGetInform.h"

#pragma once
class CObj
{

private:
	string name;
	string description;

public:
	CObj();
	CObj(const CObj &in_object);
	CObj(const string set_name, const string set_description);
	virtual ~CObj();

public:
	virtual void setName(const string &new_name);
	virtual void setDescription(const string &new_description);

	virtual string getName() const;
	virtual string getDescription() const;


public: virtual map<string, string> *readOptions();

public:
	virtual string getObjInfo();
};

