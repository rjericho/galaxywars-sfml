#pragma once
#include <iostream>
#include <string>

#include "Obj.h"

#include "IDuplicator.h"
#include "IGetInform.h"

using namespace std;
class CSpaceship :
	public CObj,
	public IDuplicator,
	public IGetInform
{
private:
	int armor;
	int speed;
	int type;

public:
	CSpaceship();
	CSpaceship(const CSpaceship &value);
	CSpaceship(CObj *Base_object, int armor_value, int speed_value);
	virtual ~CSpaceship();

public:
	virtual void setArmor(int value);
	virtual void setSpeed(int value);
	virtual void setType(int value);

	virtual int getArmor() const;
	virtual int getSpeed() const;
	virtual int getType() const;

public: virtual map< string, string > *readOptions();

public:
	virtual string getObjInfo();
	virtual CObj* makeDuplicate();

public:		virtual CSpaceship& operator--(); 
};

