#include "Battleship.h"


CBattleship::CBattleship() : CSpaceship()
{
	this->setType(1);
	this->setDamage(1);
	this->setShootSpeed(1);
}

CBattleship::CBattleship(const CBattleship &value) : CSpaceship(value)
{
	this->setType(1);
	this->setDamage(value.getDamage());
	this->setShootSpeed(value.getShootSpeed());
}

CBattleship::CBattleship(CSpaceship *value, int new_damage, int new_shootspeed) : CSpaceship(*value)
{
	this->setType(1);
	this->setDamage(new_damage);
	this->setShootSpeed(new_shootspeed);
}

CBattleship::~CBattleship()
{

}

void CBattleship::setDamage(const int value)
{
	this->damage = value;
}

void CBattleship::setShootSpeed(const int value)
{
	this->shootSpeed = value;
}

int CBattleship::getDamage() const
{
	return this->damage;
}

int CBattleship::getShootSpeed() const
{
	return this->shootSpeed;
}

map <string, string> *CBattleship::readOptions()
{
	map <string, string> * file = ((CSpaceship*)this)->readOptions();
	this->setDamage(stoi(file->find("#Damage")->second));
	this->setShootSpeed(stoi(file->find("#ShootSpeed")->second));
	return file;
}

string CBattleship::getObjInfo()
{
	return CSpaceship::getObjInfo() + "#ObjectDamage: " + to_string(this->getDamage()) + "#ObjectShootSpeed: " + to_string(this->getShootSpeed()) + "\n";
}

CSpaceship* CBattleship::makeDuplicate()
{
	CSpaceship *duplicated = new CBattleship(*this);
	return duplicated;
}

CBattleship operator+(const CBattleship& first, const CBattleship& second)
{
	CBattleship *twice = new CBattleship(first); 
	twice->setDamage(twice->getDamage() + second.getDamage());
	twice->setArmor(twice->getArmor() + second.getArmor());
	return *twice;
}