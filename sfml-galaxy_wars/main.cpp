#pragma once

#include "Planet.h"
#include "Battleship.h"
#include "Transportship.h"

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include <iostream>
using namespace std;


CPlanet *planet1;
CBattleship *battleship1;
CBattleship *battleship2;
CBattleship *battleship3;
CTransportship *transportship1;
CTransportship *transportship2;

int main()
{
	planet1 = new CPlanet();
	cout << planet1->planetShipsInfo();

	transportship1 = new CTransportship(new CSpaceship(new CObj("Transportship1", "desq"), 2, 2), 90);
	transportship2 = new CTransportship(new CSpaceship(new CObj("Transportship2", "desq"), 2, 2), 90);
	battleship1 = new CBattleship(new CSpaceship(new CObj("Battleship1", "Desq"), 3, 3), 1, 2);
	battleship2 = new CBattleship(new CSpaceship(new CObj("Battleship2", "Desq"), 3, 3), 1, 2);
	battleship3 = new CBattleship(new CSpaceship(new CObj("Battleship3", "Desq"), 3, 3), 1, 2);

	planet1->putShip(battleship1);
	planet1->putShip(transportship1);
	planet1->putShip(battleship2);
	planet1->putShip(battleship3);
	planet1->putShip(transportship2);
	
	cout << "WITHOUT SORT" << endl;
	cout << planet1->planetShipsInfo();
	cout << "WITH SORT" << endl;
	planet1->sortAll();
	cout << planet1->planetShipsInfo();
	cout << "COUNTER" << endl;
	cout << planet1->getTypes();
	planet1->deleteShipByName("Battleship3");
	cout << "DELETED BATTLESHIP3" << endl;
	cout << planet1->planetShipsInfo();
	
	delete planet1;


	/* Just SFML stuff

	sf::RenderWindow window(sf::VideoMode(200, 200), "SFML works!");
	sf::CircleShape shape(100.f);
	shape.setFillColor(sf::Color::Red);

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		window.clear();
		window.draw(shape);
		window.display();
	}

	*/
	system("pause");
	return 0;
}