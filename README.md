# README #

This README would normally document whatever steps are necessary to get your application up and running and some information.

### What is this repository for? ###

* This is my repository for simple 2D game project, which i will use as course project (Object-oriental programming)
* 1.0.0.
* Name: Galaxy Wars
* Description: Two races are fighting for survival in the galaxy. Each race has battle-, transport-, detectors ships and bonuses for them. There are n planets in the galaxy, each planet has some resources for survival. Race which put more transport-ships than other race - owner of the planet. When battle-ship meet with other ship, the battle begin. Each ship has armor and speed. Battleship has damage and shoot speed. Transportship has amount of people. Detectors has radius of vision for detect invisibility ships. Winner is a race, which destroyed other race.

### Here for code? ###

* Microsoft Visual Studio 2013 SP3
* SFML (http://sfml-dev.org/)
* C++

You can start .exe file of game for play. Or you can use .sln file for check source. 
NOTICE: for correct compilation in visual studio, you must have SFML files in C:\SFML-2.1\ . Or you can set up LINKER for your computer.
For WAT? questions read "SFML and Visual Studio": http://sfml-dev.org/tutorials/2.1/

### Author ###

* You can ask some question or make some useful tip romgalkin23@gmail.com or right here by comment use.